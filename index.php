﻿<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>WeCreate TREND</title>
    <link rel="shortcut icon" href="resource/img/fav16.png">

    <link rel="stylesheet" href="resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/css/landPage.css"><!--lead---->
    <link rel="stylesheet" href="resource/css/themify-icons.css">
    <link rel="stylesheet" href="resource/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="resource/css/normalise.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="resource/css/animate.css">
    <link rel="stylesheet" href="resource/css/lightbox.css">
    <link rel="stylesheet" href="resource/css/owl.carousel.min.css">
    <link rel="stylesheet" href="resource/css/owl.theme.default.min.css">
</head>
<body id="scroll_home">
<?php include_once "resource/particles/loader.php"; ?>

<!--/****************************** navigation *********************************/-->
<?php include_once "resource/particles/nav.php"; ?>

<!--/****************************** Slider *********************************/-->
<?php include_once "resource/particles/landSlide.php"; ?>

    <div class="container welcomNote">
        <h1 class="col-md-6">Hello</h1>
        <blockquote class="col-md-6">
            <h1 style="">WELCOME</h1>
            <span>WE CREATE TREND LTD.</span> Buying Services is a highly renowned company engaged in <span>Sourcing, Quality Management, Inspection Services.</span> We act as a bridge between <span>buyers and manufacturers</span> to make import and export activities on quality textiles on time.
        </blockquote>
    </div>
<!--/*********************************************** products *******************************************************************/-->
<section id="scroll_product">
    <div class="whitespace clearfix">
        <p>
            <i class="fa fa-tags"></i> Products
        </p>
    </div>

    <ul class="container-fluid mini-links">
        <li><a href="#scroll_knit">Knit</a></li>
        <li><a href="#scroll_woven">Woven</a></li>
        <li><a href="#scroll_sweater">Sweater</a></li>
    </ul>
        <div class="container  viewGalley clearfix" id="scroll_knit">
            <div class="itemholder clearfix" style="margin: 20px auto; text-align: right">
                <div class="productBumper">
                    <p class="lead" style="display: inline-block">Knit<br>
                        <small>Lorem ipsum dolor sit amet, consectetur</small>
                    </p>
                    <img src="resource/img/knit.png" class="img-responsive img-rounded" alt="knit main thumb">
                </div>
                <ul style="list-style: none" class="clearfix image-box" id="knitGrid">
                    <li><img src="resource/img/knit/thumbKnit.png" class="img-responsive" alt="knit thumbnail"></li>
                    <li><a href="resource/img/knit/1.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><a href="resource/img/knit/2.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/2.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><a href="resource/img/knit/3.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/3.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><a href="resource/img/knit/4.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/4.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><a href="resource/img/knit/5.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/5.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><a href="resource/img/knit/6.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/6.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><a href="resource/img/knit/7.png" data-lightbox="knit" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/7.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    <li><i class="ti-image"></i><h4><i class="fa fa-plus-circle fa-2x" style="color: #c0a16b"></i><a href="knitwears.php" target="_blank">More</a></h4></li>
                </ul>
            </div>
        </div>

        <div class="container  viewGalley clearfix" id="scroll_woven">
            <div class="itemholder clearfix" style="margin: 20px auto">
                <div class="productBumper clearfix" style="text-align: left">
                    <img src="resource/img/woven.png" class="img-responsive img-rounded" alt="Woven main thumbnail">
                    <p class="lead" style="display: inline-block">Woven<br>
                        <small>Lorem ipsum dolor sit amet, consectetur</small>
                    </p>
                </div>
                <ul style="list-style: none" class="clearfix image-box" id="wovenGrid">
                    <li><img src="resource/img/woven/thumbWoven.png" class="img-responsive" alt="thumbnail woven"></li>
                    <li><a href="resource/img/woven/1.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/1.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><a href="resource/img/woven/2.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/2.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><a href="resource/img/woven/3.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/3.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><a href="resource/img/woven/4.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/4.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><a href="resource/img/woven/5.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/5.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><a href="resource/img/woven/6.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/6.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><a href="resource/img/woven/7.png" data-lightbox="woven" data-title="Garment: woven"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/woven/7.png" class="img-responsive" alt="thumbnail woven"></a></li>
                    <li><i class="ti-image"></i><h4><i class="fa fa-plus-circle fa-2x" style="color: #c0a16b"></i><a href="wovenwears.php" target="_blank">More</a></h4></li>
                </ul>
            </div>
        </div>
    <div class="container  viewGalley clearfix" style="padding-bottom: 70px" id="scroll_sweater">
        <div class="itemholder clearfix" style="margin: 20px auto; text-align: right">
            <div class="productBumper clearfix">
                <p class="lead" style="display: inline-block">Sweater<br>
                    <small>Lorem ipsum dolor sit amet, consectetur</small>
                </p>
                <img src="resource/img/sweater.png" class="img-responsive img-rounded" alt="Sweater main thumbnail">
            </div>
            <ul style="list-style: none" class="clearfix image-box" id="sweaterGrid">
                <li><img src="resource/img/sweater/thumbSweater.png" class="img-responsive" alt="thumb sweater"></li>
                <li><a href="resource/img/sweater/1.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/1.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><a href="resource/img/sweater/2.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/2.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><a href="resource/img/sweater/3.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/3.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><a href="resource/img/sweater/4.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/4.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><a href="resource/img/sweater/5.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/5.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><a href="resource/img/sweater/6.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/6.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><a href="resource/img/sweater/7.png" data-lightbox="sweater" data-title="Garment: Sweater"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/sweater/7.png" class="img-responsive" alt="thumb sweater"></a></li>
                <li><i class="ti-image"></i><h4><i class="fa fa-plus-circle fa-2x" style="color: #c0a16b"></i><a href="">More</a></h4></li>
            </ul>
        </div>
    </div>

    <ul class="container-fluid mini-links">
        <li><a href="#scroll_knit">Knit</a></li>
        <li><a href="#scroll_woven">Woven</a></li>
        <li><a href="#scroll_sweater">Sweater</a></li>
    </ul>
</section>
<!--/**************************************************** Products End ******************************************************/-->


<!-- /************************************************* Short about *********************************************************/-->

<section id="scroll_about" class="bottomShade">
    <div class="container-fluid" style="padding: 0">
        <div class="whitespace clearfix">
            <p>
                <i class="fa fa-group"></i> About
            </p>
        </div>
        <div class="container shortAbout clearfix">
            <div class="note col-md-12" >
                <h1><img src="resource/img/about/idea.svg" class="img-responsive" alt="icon"></h1>
                <h2>Company Philosophy</h2>
                <p style="text-align: center">
                    <span class="ext-lg">A</span>t WE CREATE TREND LTD. we promote a simple philosophy to do more with how to be the best company and belief that <b>our employees are our greatest asset</b>.
                </p>
            </div>
            <div class="note col-md-6">
                <h1><img src="resource/img/about/trophy.svg" class="img-responsive" alt="icon"></h1>
                <h2>Winning Edge</h2>
                <p>
                    <span class="ext-lg" style="padding: 0">W</span>E CREATE TREND LTD. has strong hold on material sourcing from both <b>far-east and local</b>, which to be competitive on deliveries and price. Our design and development team’s
                    <b>relentless research</b> leaves the customers on better options with <b>Yarn/fabrics and quick development on trims</b>. We have our own <b>sampling departments</b> with all required facilities which enable us to provide our clients <b>required sampling on time</b>.
                </p>
            </div>
            <div class="note col-md-6">
                <h1><img src="resource/img/about/focus.svg" class="img-responsive" alt="icon"></h1>
                <h2>Prime Focus</h2>
                <p><span class="ext-lg">H</span>ere, it’s important what we do in business and how we do the business. We <b>create and encourage</b> the concept of <b>‘Ethical’ and ‘Social’</b> responsibility while <b>sourcing and manufacturing</b>. Our team of quality controller continuously work to improve the <b>standards of the factories</b> and their products. We do these exercise much prior to the order placement so that we can get the desired product.</p>
            </div>
            <div class="note col-md-12">
                <h1><img src="resource/img/about/clothes-tag.svg" class="img-responsive" alt="icon"></h1>
                <h2>Product Profile</h2>
                <p style="text-align: center">
                    <span class="ext-lg" style="padding: 0">W</span>e have a very broad product profile
                    <br>
                    and offer various kind of
                    <b>formal, sportswear, outerwear</b>
                    and value added products for
                    <br>
                    <b>Children’s, Ladies & Men’s: Sweaters made of imported as well as local yarns</b>
                    <br>
                    <b>T-shirts, Polo shirts
                        Sweat shirts, Jogging sets, Windbreakers
                        Woven bottoms, Skirts, Overalls
                        Ladies blouses, Men’s shirts</b>
                    At present we are exporting various kinds of woven / flat knitted / circular knitted garments to USA, north America, European countries and Canada.
                </p>
            </div>
        </div>
        <div class="container">
            <a href="about.php#about" class="aboutlink"><span>Know More</span> <i class="ti-angle-right"></i></a>
        </div>
    </div>
</section>
<!--/*********************************************** Shortabout End ************************************************/-->

<!--/*********************************************** Client ************************************************/-->

<section>
    <div class="container-fluid" style="padding:70px 0; background-color: #eee">

        <div class="container">
            <div class="col-md-6 client-blurb">
                <img src="resource/img/money-bag.svg" class="img-responsive" alt="icon" style="max-width: 100px; margin: 0 auto">
                <h1 class="client-header">Bank Detail</h1>
                <ul style="list-style: none">
                    <li><span style="color: #2F8ED3">Name <i class="fa fa-angle-right"></i></span> Southeast Bank Limited</li>
                    <li><span style="color: #2F8ED3">Branch <i class="fa fa-angle-right"></i></span> CDA Avenue Branch</li>
                    <li><span style="color: #2F8ED3">Address <i class="fa fa-angle-right"></i></span> Ali Villa, 1640/1861 (New), CDA Avenue, Asian Highway,East Nasirabad, Chittagong, Bangladesh</li>
                    <li><span style="color: #2F8ED3">SWIFT: SEBDBDDHACDA</span></li>
                </ul>
            </div>
            <div class="col-md-6 client-blurb">
            <img src="resource/img/buyer.svg" class="img-responsive" alt="icon" style="max-width: 100px; margin: 0 auto">
            <h1 class="client-header">Major Buyers</h1>
            <ul style="list-style: none">
                <li><span style="color: #2F8ED3">Canada <i class="fa fa-angle-right"></i></span> YM</li>
                <li><span style="color: #2F8ED3">Mexico <i class="fa fa-angle-right"></i></span> coppel</li>
                <li><span style="color: #2F8ED3">U.K. <i class="fa fa-angle-right"></i></span> Double two</li>
                <li><span style="color: #2F8ED3">Germany <i class="fa fa-angle-right"></i></span> Kik-textilien etc.</li>
                <li><span style="color: #2F8ED3">Belgium <i class="fa fa-angle-right"></i></span> Vegotex, Hanes etc.</li>
                <li><span style="color: #2F8ED3">USA <i class="fa fa-angle-right"></i></span> Marvyns , Green dog , carters etc.</li>
                <li><span style="color: #2F8ED3">france <i class="fa fa-angle-right"></i></span> Defimode , Sodiref , Vetura samino etc.</li>
            </ul>
        </div>
    </div>

    <div class="container-fluid" style="padding: 0;">
        <div class="">
            <ul class="owl-carousel" id="Clients" style="list-style: none; margin: 0 auto">
                <li class="slideItem">
                    <div class="client-logo">
                        <img src="resource/img/Clients/carter's.png" class="img-responsive" alt="carter's">
                    </div>
                </li>
                <li class="slideItem">
                    <div class="client-logo">
                        <img src="resource/img/Clients/double2.png" class="img-responsive" alt="double2">
                    </div>
                </li>
                <li class="slideItem">
                    <div class="client-logo">
                        <img src="resource/img/Clients/kik.jpg" class="img-responsive" alt="kik">
                    </div>
                </li>
                <li class="slideItem">
                    <div class="client-logo">
                        <img src="resource/img/Clients/vegotex.png" class="img-responsive" alt="vegotex">
                    </div>
                </li>
                <li class="slideItem">
                    <div class="client-logo">
                        <img src="resource/img/Clients/YM.png" class="img-responsive" alt="YM">
                    </div>
                </li>
                <li class="slideItem">
                    <div class="client-logo">
                        <img src="resource/img/Clients/coppel.png" class="img-responsive" alt="coppel">
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
<!--/*********************************************** Client End ************************************************/-->


<!--/*********************************************** Contact **************************************************/-->
<?php include_once "resource/particles/contact.php"; ?>


<!--/********************************************* Contact End ************************************************************/-->
<?php include_once "resource/particles/footer.php"; ?>

    <script src="resource/js/jquery.min.js"></script>
    <script src="resource/js/jquery.mousewheel.min.js"></script>
    <script src="resource/js/norms.js"></script>
    <script src="resource/js/bootstrap.min.js"></script>
    <script src="resource/js/lightbox.js"></script>
    <script src="resource/js/landing.js"></script>
    <script src="resource/js/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function() {
          lightbox.option({
              'disableScrolling': true
          });

            var owl = $('.owl-carousel');
            owl.owlCarousel({
                loop: true,
                items: 1,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 1500,
                autoplayHoverPause: false,
                autoplaySpeed: 500,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    1000: {
                        items: 3,
                        nav: false,
                        loop: true
                    }
                }
            });
            owl.owlCarousel();
        });
    </script>
    <script type="text/javascript">
        var isChrome = !!window.chrome && !!window.chrome.webstore;
        if(isChrome){
          $('.whitespace').css({
            '-webkit-transform' : 'translate3d(0,0,0)',
            'transform' : 'translate3d(0,0,0)'
          })
          $('.topNav.invisible').css({
            'height' : '0'
          })
        }
    </script>
</body>
</html>
