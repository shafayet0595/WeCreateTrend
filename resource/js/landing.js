$(document).ready(function(){

  $(window).on('scroll',function() {

    var wScroll = $(this).scrollTop();

    if(wScroll > $('.welcomNote').offset().top -($(window).height()/1.4)){
        $('.welcomNote>h1').addClass('animated fadeIn');
        $('.welcomNote>blockquote>h1').addClass('animated fadeInRight');
    }


    if(wScroll > $('#knitGrid').offset().top -($(window).height()/1.2)){
        $("#knitGrid").find('li').each(function (eachIndex) {
            setTimeout(function () {
                $('#knitGrid li').eq(eachIndex).addClass("animated fadeInUp");
            }, 130 * (eachIndex+1));
        });
    }

    if(wScroll > $('#wovenGrid').offset().top -($(window).height()/1.2)){
        $('#wovenGrid').find('li').each(function (eachIndex) {
            setTimeout(function () {
                $('#wovenGrid li').eq(eachIndex).addClass("animated fadeInUp");
            }, 130 * (eachIndex+1));
        });
    }
    if(wScroll > $("#sweaterGrid").offset().top -($(window).height()/1.2)){
        $('#sweaterGrid').find('li').each(function (eachIndex) {
            setTimeout(function () {
                $('#sweaterGrid li').eq(eachIndex).addClass("animated fadeInUp");
            }, 130 * (eachIndex+1));
        });
    }

  });

});
