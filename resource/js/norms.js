(function ($) {
  'use strict';

  $.srSmoothscroll = function (options) {
    // defaults
    var self = $.extend({
      step: 55,
      speed: 600,
      ease: 'swing',
      target: $('body'),
      container: $('window')
    }, options || {});

    // private fields & init
    var container = self.container, top = 0 , step = self.step , viewport = container.height() , wheel = true;
    var target = (self.target.selector == 'body') ? ((navigator.userAgent.indexOf('AppleWebKit') !== -1) ? self.target : $('html')) : container;
    // events
    self.target.mousewheel(function (event, delta) {
      wheel = true;
      if (delta < 0) // down
        top = (top+viewport) >= self.target.outerHeight(true) ? top : top+=step;
      else // up
        top = top <= 0 ? 0 : top-=step;
      target.stop().animate({scrollTop: top}, self.speed, self.ease, function () {
        wheel = false;
      });
      // return false;
    });
    container
      .on('resize', function (e) {viewport = container.height();})
      .on('scroll', function (e) {
        if (!wheel)
          top = container.scrollTop();
      });
  };
})(jQuery);

$(function () {
  $.srSmoothscroll({
    // defaults
    step: 55,
    speed: 700,
    ease: 'swing',
    target: $('body'),
    container: $(window)
  });
});

$(window).on('load',function(){
    $('#overlay_loader').hide();
    $('body').css({
        "overflow":"auto"
    });
});

$(document).ready(function () {
    if ($('#overlay_loader').is(':visible')) {
        $('body').css({
            "overflow":"hidden"
        });
    }else{
        $('body').css({
            "overflow":"auto"
        });
    }

    $(".menuBar").on('click',function (m) {
        m.preventDefault();
        $('.floated_Nav_links').toggleClass("float_menu");
        $('.respoBodyholder').toggleClass("show");
        $('html').css({
            "overflow":"hidden"
        });
    });
    $('.remove_floater').on('click',function (f) {
        f.preventDefault();
        $('.floated_Nav_links').toggleClass("float_menu");
        $('.respoBodyholder').toggleClass("show");
        $('html').css({
            "overflow":"auto"
        });
    });
    $('.respoBodyholder').on('click',function () {
        $('.floated_Nav_links').toggleClass("float_menu");
        $('.respoBodyholder').toggleClass("show");
        $('html').css({
            "overflow":"auto"
        });
    });
    $(function(){
        var navMain = $(".floated_Nav_links"); // avoid dependency on #id
        // "a:not([data-toggle])" - to avoid issues caused
        // when you have dropdown inside navbar
        navMain.on("click", "a:not([data-toggle])", null, function () {
            navMain.toggleClass('float_menu');
            $('.respoBodyholder').toggleClass("show");
            $('html').css({
                "overflow":"auto"
            });
        });
    });

    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight()+15,
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
            var item = $($(this).attr("href"));
            if (item.length) { return item; }
        });

// Bind click handler to menu items
// so we can get a fancy scroll animation
    menuItems.click(function(e){
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 700);
        e.preventDefault();
    });

// Bind to scroll
    $(window).on('scroll',function(){

      var pointer =$(".navigation");
      var topNav =$(".topNav");

      if(($(window).scrollTop() > 15)){
          pointer.addClass("fixed_nav");
          topNav.addClass('invisible');
      }else{
          pointer.removeClass("fixed_nav");
          topNav.removeClass('invisible');
      }
        // Get container scroll position
        var fromTop = $(this).scrollTop() + topMenuHeight;

        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass("active")
                .end().filter("[href='#" + id + "']").parent().addClass("active");
        }
    });
});
