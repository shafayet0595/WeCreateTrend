<section id="scroll_contact">
    <div class="whitespace clearfix">
        <p>
            <i class="fa fa-comments"></i> Contact
        </p>
    </div>
    <div class="container-fluid bottomShade" style="padding:0; overflow: hidden">
        <div class="contactFormWrap container">
            <div class="col-md-6 formHolder">
                <form action="">
                    <input type="text" placeholder="Name">
                    <input type="text" placeholder="Email">
                    <textarea name="" id="" cols="30" rows="10" placeholder="Message"></textarea>
                    <input type="submit" value="SEND">
                </form>
            </div>
            <div class="col-md-6 blurb">
                <h2>Keep in touch</h2>
                <h1 style="text-align: center">
                    <img src="resource/img/telephone.png" class="img-responsive" alt="icon" style="max-width: 180px; margin: 20px auto">
                </h1>
                <p class="lead" style="padding: 20px; border-top:2px solid #eee; color: #5e5e5e ">
                    What do you like about our products? What products do you want in the future? Contact us and let us know!
                </p>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="padding:0; background-color: #eee; overflow: hidden">
        <div class="multiAddress container">
            <div class="col-md-4">
                <h4>Bangladesh</h4>
                <p>39 Hossain Shahid Sarwardi Road,
                    kotwali, Chittagong,
                    Bangladesh.</p>
                <ul>
                    <li>Tel : +880-31-622804</li>
                    <li>Fax : +880-31-624547</li>
                    <li>Mail : info@wecreatetrend.com </li>
                </ul>

            </div>
            <div class="col-md-4">
                <h4>USA</h4>
                <p>178-10 Wexford Terrace,
                    Apt # 6 B, Jamaica,NY 11432</p>
                <ul>
                    <li>Enayet Karim Saqui</li>
                    <li>Tel : +1 916-585-0427</li>
                    <li>Fax : 5862611327</li>
                    <li>Mail : wct-usa@wecreatetrend.com</li>
                </ul>
            </div>
            <div class="col-md-4">
                <h4>Canada</h4>
                <p>
                    290 Parkside Drive, Unit 10, Fredericton, New Brunswick E3B 5V7, Canada.
                </p>
                <ul>
                    <li>Dr. Bedarul Alam, <small>(Director Marketing)</small></li>
                    <li>Tel : 001 506 206 2005</li>
                    <li>Mail : wct-ca@wecreatetrend.com</li>
                </ul>
            </div>
        </div>
    </div>
</section>