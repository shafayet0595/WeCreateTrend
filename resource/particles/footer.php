<footer class="container-fluid" style="padding;0; overflow: hidden">
    <div class="container" style="padding: 70px 15px;">
        <div class="col-lg-4">
            <h3>Socials</h3>
            <ul class="social">
                <li><a href=""><i class="fa fa-facebook-f"></i></a></li>
                <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                <li><a href=""><i class="fa fa-skype"></i></a></li>
                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div>
        <div class="col-md-6">
            <h3>Address</h3>
            <ul class="address">
                <li>
                    <i class="fa fa-map-o"></i> 39 Hossain Shahid Sarwardi Road,
                    kotwali, Chittagong,
                    Bangladesh.
                </li>
            </ul>
            <h3>Contact</h3>
            <ul class="address">
                <li>
                    <i class="fa fa-envelope"></i> info@wecreatetrend.com
                </li>
                <li>
                    <i class="fa fa-fax"></i> +880-31-624547
                </li>
                <li>
                    <i class="fa fa-phone"></i> +880-31-622804
                </li>
            </ul>
        </div>
        <div class="col-md-2">
            <h3>Essentials</h3>
            <ul class="links">
                <li><a href="">Home</a></li>
                <li><a data-toggle="collapse" href="#essentialLinks" aria-expanded="false" aria-controls="essentialLinks">Products <i class="fa fa-angle-down"></i></a>
                    <ul class="collapse" id="essentialLinks">
                        <li><a href="knitwears.php">Knit</a></li>
                        <li><a href="wovenwears.php">Woven</a></li>
                        <li><a href="">Sweaters</a></li>
                        <li><a href="">Others</a></li>
                    </ul>
                </li>
                <li><a href="">Services</a></li>
                <li><a href="">About</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </div>
    </div>
    <div class="container-fluid footerBlurb">
        <ul style="list-style: none; text-align: center" class="col-md-6">
            <li>All rights reserved © 2017</li>
        </ul>

        <ul style="list-style: none" class="col-md-6">
            <li> <i class="fa fa-code"></i> developed by <a href="www.codencrayon.com">CodenCrayon</a></li>
        </ul>
    </div>
</footer>
