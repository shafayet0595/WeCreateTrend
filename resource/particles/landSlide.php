<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators caro_nav">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner carouselOverride" role="listbox">
        <div class="item active">
            <img src="resource/img/slider/slider1.png" class="img-responsive animated fadeIn" alt="slider1 image">
            <div class="carousel-caption">
                <blockquote style="text-align: center" class="animated fadeInUp">
                    <h1 class="animated fadeIn"><i class="ti-star"></i></h1>
                    <i class="fa fa-quote-left" style="color: #c0a16b"></i> Trendy is the last stage before tacky. <i class="fa fa-quote-right" style="color: #c0a16b"></i><br>
                    <small style="color: #fff; text-transform:capitalize; display: inline-block"> <i>Karl Lagerfeld</i></small>
                </blockquote>
            </div>
        </div>
        <div class="item">
            <img src="resource/img/slider/slider2.png" class="img-responsive animated fadeIn" alt="slider2 image">
            <div class="carousel-caption">
                <blockquote style="text-align: center" class="animated fadeInUp">
                    <h1  class="animated fadeIn"><i class="ti-crown"></i></h1>
                    <i class="fa fa-quote-left" style="color: #c0a16b"></i> Clothes make the man. Naked people have little or no influence on society. <i class="fa fa-quote-right" style="color: #c0a16b"></i><br>
                    <small style="color: #fff; text-transform:capitalize; display: inline-block"> <i>Mark Twain</i></small>
                </blockquote>
            </div>
        </div>

        <div class="item">
            <img src="resource/img/slider/slider3.png" class="img-responsive animated fadeIn" alt="slider3 image">
            <div class="carousel-caption">
                <blockquote style="text-align: center" class="animated fadeInUp">
                    <h1 class="animated fadeIn"><i class="fa fa-sun-o"></i></h1>
                    <i class="fa fa-quote-left" style="color: #c0a16b"></i> Sweater, n. Garment worn by child when its mother is feeling chilly. <i class="fa fa-quote-right" style="color: #c0a16b"></i><br>
                    <small style="color: #fff; text-transform:capitalize; display: inline-block"> <i>Ambrose Bierce</i></small>
                </blockquote>
            </div>
        </div>

    </div>
</div>
