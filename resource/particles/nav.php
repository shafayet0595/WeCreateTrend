<div class="floated_Nav_links clearfix visible-xs">
    <a href="" class="remove_floater" data-toggle="removeFloater"><i class="ti-close"></i></a>
    <ul class="">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a data-toggle="collapse" href="#extlinks" aria-expanded="false" aria-controls="extlinks">Products <i class="fa fa-angle-down"></i></a>
            <ul class="collapse" id="extlinks">
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="index.php#scroll_contact" id="profoff">Contact</a></li>
    </ul>
    <div class="container-fluid topNav" style="text-align: center; border: none">
        <ul class="col-md-6 col-sm-6">
            <li>
                <i class="fa fa-phone"></i> +880-31-622804
            </li>
            <li>
                <i class="fa fa-fax"></i> +880-31-624547
            </li>
            <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
        </ul>
        <ul class="col-md-6 col-sm-6">
            <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                kotwali, Chittagong,
                Bangladesh.</li>
        </ul>
    </div>
</div>
<div class="respoBodyholder visible-xs"></div>
<div class="container-fluid topNav hidden-xs">
    <ul class="col-md-6 col-sm-6" style="text-align: left">
        <li>
            <i class="fa fa-phone"></i> +880-31-622804
        </li>
        <li>
            <i class="fa fa-fax"></i> +880-31-624547
        </li>
        <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
    </ul>
    <ul class="col-md-6 col-sm-6" style="text-align: right">
        <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
            kotwali, Chittagong,
            Bangladesh.</li>
    </ul>
</div>
<div class="container-fluid navigation clearfix">
    <div class="logoHolder">
        <a href="" class="menuBar visible-xs">
            <span></span>
        </a>
        <img src="resource/img/colorLogo.png" alt="WCT logo">
    </div>
    <ul class="navigationHold pull-right hidden-xs" id="top-menu">
        <li class="active"><a class="page-scroll" href="#scroll_home">Home</a></li>
        <li class="productList">
            <a class="page-scroll" href="#scroll_product">Products</a>
            <ul>
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php#service">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="#scroll_about">About</a></li>
        <li><a class="page-scroll" href="#scroll_contact">Contact</a></li>
    </ul>
</div>
