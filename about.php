<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WeCreate TREND</title>
    <link rel="shortcut icon" href="resource/img/fav16.png">
    <link rel="stylesheet" href="resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/css/landPage.css"><!--lead---->
    <link rel="stylesheet" href="resource/css/themify-icons.css">
    <link rel="stylesheet" href="resource/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="resource/css/normalise.css">
    <link rel="stylesheet" href="resource/css/animate.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
</head>
<body id="scroll_home">
<?php include_once "resource/particles/loader.php"; ?>
<div class="floated_Nav_links clearfix visible-xs">
    <a href="" class="remove_floater" data-toggle="removeFloater"><i class="ti-close"></i></a>
    <ul class="">
        <li><a href="index.php">Home</a></li>
        <li><a data-toggle="collapse" href="#extlinks" aria-expanded="false" aria-controls="extlinks">Products <i class="fa fa-angle-down"></i></a>
            <ul class="collapse" id="extlinks">
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li class="active"><a href="">About</a></li>
        <li><a href="index.php#scroll_contact">Contact</a></li>
    </ul>
    <div class="container-fluid topNav" style="text-align: center; border: none">
        <ul class="col-md-6 col-sm-6">
            <li>
                <i class="fa fa-phone"></i> +880-31-622804
            </li>
            <li>
                <i class="fa fa-fax"></i> +880-31-624547
            </li>
            <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
        </ul>
        <ul class="col-md-6 col-sm-6">
            <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                kotwali, Chittagong,
                Bangladesh.</li>
        </ul>
    </div>
</div>
<div class="respoBodyholder visible-xs"></div>
<div class="container-fluid topNav hidden-xs">
    <ul class="col-md-6 col-sm-6" style="text-align: left">
        <li>
            <i class="fa fa-phone"></i> +880-31-622804
        </li>
        <li>
            <i class="fa fa-fax"></i> +880-31-624547
        </li>
        <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
    </ul>
    <ul class="col-md-6 col-sm-6" style="text-align: right">
        <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
            kotwali, Chittagong,
            Bangladesh.</li>
    </ul>
</div>
<div class="container-fluid navigation clearfix">
    <div class="logoHolder">
        <a href="" class="menuBar visible-xs">
            <span></span>
        </a>
        <img src="resource/img/colorLogo.png" alt="WCT logo">
    </div>
    <ul class="navigationHold pull-right hidden-xs" id="top-menu">
        <li><a class="page-scroll" href="index.php#scroll_home">Home</a></li>
        <li class="productList">
            <a class="page-scroll" href="index.php#scroll_product">Products</a>
            <ul>
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php#service">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li class="active"><a href="#scroll_home">About</a></li>
        <li><a class="page-scroll" href="index.php#scroll_contact">Contact</a></li>
    </ul>
</div>

<!--/****************************** header *********************************/-->
<section class="container-fluid tagContainer" style="padding: 0;">
    <img src="resource/img/pattern.png" alt="header image">
    <h1><i class="ti-write"></i> About</h1>
    <h5>home / about</h5>
</section>

<section class="container-fluid" style="padding:0" id="about">
    <div class="container" style="padding: 70px 15px">
        <div class="aboutStory col-md-9 col-sm-9">
            <div class="story">
                <h3>Introduction</h3>
                <p>
                    <b>WE CREATE TREND LTD..</b> – A completely equipped <b>export oriented buying house</b> based in Bangladesh. It’s vision to create a new ERA in the sector of buying house by sourcing <b>smarter factories, maintaining desired qualities, matching competitive prices,  on time shipments etc</b>. WE CREATE TREND LTD.. believes to continue exploring new ideas till developing and changing itself with buyer’s                       requirement.WE CREATE TREND LTD.. really
                    <br>
                    <b>welcomes you being ‘YOUR GLOBAL FRIEND’</b>
                </p>
            </div>


            <div class="story">
                <h3>Restless journey</h3>
                <p>
                    WE CREATE TREND LTD.. started its journey <b>since 2004</b> as a Limited Company <br>
                    - Aimed to <b>source & supply</b> quality garments in both <b>woven and knits</b> with the support of reliable factories from Bangladesh. A journey which is started with a small team members – one account and <b>today we have members who are involved in handling production of different buyer and also busy with developments of  new accounts.</b> The company’s young & passionate team leaders always devoted to  <b>improve its product and service day by day</b> – which keeps the team on toes to perform.
                </p>
            </div>


            <div class="story">
                <h3>Belief became Mission</h3>
                <p>
                    At WE CREATE TREND LTD.., we believe that it is <b>customer’s right to have quality product at a competitive price, delivered at the right time</b>. We also believe in race <b>to face the ever-changing business needs</b>. We turned these beliefs into our mission and our team is dedicated to uphold this mission.
                </p>
            </div>

        </div>

        <div class="sideInfo col-md-3 col-sm-3">
            <h4><i class="ti-info-alt"></i> Contact Info.</h4>
            <ul style="padding: 0;">
                <li>
                    <i class="fa fa-map-o"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                    kotwali, Chittagong,
                    Bangladesh.
                </li>
                <li>
                    <i class="fa fa-envelope"></i>&nbsp; info@wecreatetrend.com
                </li>
                <li>
                    <i class="fa fa-fax"></i>&nbsp; +880-31-624547
                </li>
                <li>
                    <i class="fa fa-phone"></i>&nbsp; +880-31-622804
                </li>
            </ul>


            <h4><i class="ti-tag"></i> Major Buyers</h4>
            <ul style="padding:0; list-style: none">
                <li> <b>Canada</b> <i class="fa fa-angle-right"></i> YM</li>
                <li> <b>Mexico</b> <i class="fa fa-angle-right"></i> coppel</li>
                <li> <b>U.K.</b> <i class="fa fa-angle-right"></i> Double two</li>
                <li> <b>Germany</b> <i class="fa fa-angle-right"></i> Kik-textilien etc.</li>
                <li> <b>Belgium</b> <i class="fa fa-angle-right"></i> Vegotex, Hanes etc.</li>
                <li> <b>USA</b> <i class="fa fa-angle-right"></i> Marvyns , Green dog , carters etc.</li>
                <li> <b>france</b> <i class="fa fa-angle-right"></i> Defimode , Sodiref , Vetura samino etc.</li>
            </ul>

            <h4 class="client-header"> <i class="ti-wallet"></i> Bank Detail</h4>
            <ul style="padding: 0;">
                <li><b>Name <i class="fa fa-angle-right"></i> </b> Southeast Bank Limited</li>
                <li><b>Branch <i class="fa fa-angle-right"></i> </b> CDA Avenue Branch</li>
                <li><b>Address <i class="fa fa-angle-right"></i>  </b> Ali Villa, 1640/1861 (New), CDA Avenue, Asian Highway,East Nasirabad, Chittagong, Bangladesh</li>
                <li><b>SWIFT: SEBDBDDHACDA</b></li>
            </ul>
        </div>
    </div>

</section>


<?php include_once "resource/particles/footer.php";?>

<script src="resource/js/jquery.min.js"></script>
<script src="resource/js/jquery.mousewheel.min.js"></script>
<script src="resource/js/norms.js"></script>
<script src="resource/js/bootstrap.min.js"></script>
<script>
$(window).on('scroll',function() {
    var wScroll = $(this).scrollTop();
    $('.tagContainer>img').css({
        '-webkit-transform': 'translate(0,-'+ wScroll/12 +'%)',
                'transform': 'translate(0,-'+ wScroll/12 +'%)'
    });
});
</script>
</body>
</html>
