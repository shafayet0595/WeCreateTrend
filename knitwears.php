<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WeCreate TREND</title>
    <link rel="shortcut icon" href="resource/img/fav16.png">
    <link rel="stylesheet" href="resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/css/landPage.css"><!--lead---->
    <link rel="stylesheet" href="resource/css/themify-icons.css">
    <link rel="stylesheet" href="resource/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="resource/css/normalise.css">
    <link rel="stylesheet" href="resource/css/animate.css">
    <link rel="stylesheet" href="resource/css/lightbox.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
</head>
<body id="scroll_home">
<?php include_once "resource/particles/loader.php"; ?>

<div class="floated_Nav_links clearfix visible-xs">
    <a href="" class="remove_floater" data-toggle="removeFloater"><i class="ti-close"></i></a>
    <ul class="">
        <li><a href="index.php">Home</a></li>
        <li><a data-toggle="collapse" href="#extlinks" aria-expanded="false" aria-controls="extlinks">Products <i class="fa fa-angle-down"></i></a>
            <ul class="collapse" id="extlinks">
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="index.php#scroll_contact">Contact</a></li>
    </ul>
    <div class="container-fluid topNav" style="text-align: center; border: none">
        <ul class="col-md-6 col-sm-6">
            <li>
                <i class="fa fa-phone"></i> +880-31-622804
            </li>
            <li>
                <i class="fa fa-fax"></i> +880-31-624547
            </li>
            <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
        </ul>
        <ul class="col-md-6 col-sm-6">
            <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                kotwali, Chittagong,
                Bangladesh.</li>
        </ul>
    </div>
</div>
<div class="respoBodyholder visible-xs"></div>
<div class="container-fluid topNav hidden-xs">
    <ul class="col-md-6 col-sm-6" style="text-align: left">
        <li>
            <i class="fa fa-phone"></i> +880-31-622804
        </li>
        <li>
            <i class="fa fa-fax"></i> +880-31-624547
        </li>
        <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
    </ul>
    <ul class="col-md-6 col-sm-6" style="text-align: right">
        <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
            kotwali, Chittagong,
            Bangladesh.</li>
    </ul>
</div>
<div class="container-fluid navigation clearfix">
    <div class="logoHolder">
        <a href="" class="menuBar visible-xs">
            <span></span>
        </a>
        <img src="resource/img/colorLogo.png" alt="WCT logo">
    </div>
    <ul class="navigationHold pull-right hidden-xs" id="top-menu">
        <li><a class="page-scroll" href="index.php#scroll_home">Home</a></li>
        <li class="productList active">
            <a href="#scroll_home">Products</a>
            <ul>
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a class="page-scroll" href="index.php#scroll_contact">Contact</a></li>
    </ul>
</div>

<!--/****************************** header *********************************/-->
<section class="container-fluid tagContainer" style="padding: 0;">
    <img src="resource/img/pattern.png" alt="header image">
    <h1><i class="ti-image"></i> Knit Wears</h1>
    <h5>home / Knit</h5>
</section>

<section class="container-fluid" style="padding:70px 0" id="knitgallery">
    <div class="container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-tabs-overClock" role="tablist">
            <li role="presentation" class="active"><a href="#Kid" aria-controls="messages" role="tab" data-toggle="tab">Kid</a></li>
            <li role="presentation"><a href="#Men" aria-controls="home" role="tab" data-toggle="tab">Men</a></li>
            <li role="presentation"><a href="#Women" aria-controls="profile" role="tab" data-toggle="tab">Women</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="Kid">
            <div class="viewGalley nonFloated clearfix" id="scroll_knit">
                <div class="itemholder clearfix" style="margin: 20px auto; text-align: right">
                    <ul style="list-style: none; padding:0" class="clearfix image-box" id="genericGallery">
                        <li><a href="resource/img/knit/kids/1.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/2.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/2.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/3.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/3.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/4.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/4.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/5.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/5.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/5.1.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/5.1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/6.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/6.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/7.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/7.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/8.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/8.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/9.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/9.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/10.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/10.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/11.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/11.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/12.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/12.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/13.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/13.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/14.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/14.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/15.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/15.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/16.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/16.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/17.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/17.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/18.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/18.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/19.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/19.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/20.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/20.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/21.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/21.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/22.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/22.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/23.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/23.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/23.1.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/23.1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/23.2.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/23.2.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/23.3.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/23.3.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/24.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/24.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/25.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/25.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/26.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/26.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/27.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/27.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/28.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/28.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/29.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/29.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/30.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/30.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/31.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/31.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/32.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/32.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/33.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/33.png" class="img-responsive" alt="Thumb Knit"></a></li>
                        <li><a href="resource/img/knit/kids/34.png" data-lightbox="knitKid" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/kids/34.png" class="img-responsive" alt="Thumb Knit"></a></li>
                    </ul>
                </div>
            </div>
          </div>


            <div role="tabpanel" class="tab-pane" id="Men">
                <div class="viewGalley nonFloated clearfix" id="scroll_knit">
                    <div class="itemholder clearfix" style="margin: 20px auto; text-align: right">
                        <ul style="list-style: none; padding:0" class="clearfix image-box" id="genericGallery">
                            <li><a href="resource/img/knit/men/1.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/3.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/3.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/4.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/4.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/5.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/5.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/6.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/6.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/7.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/7.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/8.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/8.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/9.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/9.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/10.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/10.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/11.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/11.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/12.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/12.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/13.1.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/13.1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/13.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/13.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/14.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/14.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/15.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/15.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/16.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/16.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/17.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/17.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/18.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/18.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/19.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/19.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/20.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/20.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/21.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/21.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/22.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/22.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/23.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/23.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/24.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/24.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/25.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/25.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/26.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/26.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/27.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/27.png" class="img-responsive" alt="Thumb Knit"></a></li>
                            <li><a href="resource/img/knit/men/28.png" data-lightbox="knitMan" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/men/28.png" class="img-responsive" alt="Thumb Knit"></a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="Women">
              <div class="viewGalley nonFloated clearfix" id="scroll_knit">
                  <div class="itemholder clearfix" style="margin: 20px auto; text-align: right">
                      <ul style="list-style: none; padding:0" class="clearfix image-box" id="genericGallery">
                        <li><a href="resource/img/knit/women/0.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/0.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/1.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/1.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/2.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/2.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/3.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/3.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/4.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/4.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/5.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/5.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/6.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/6.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/7.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/7.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/8.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/8.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/9.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/9.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/10.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/10.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/11.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/11.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/12.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/12.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/13.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/13.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/14.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/14.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/15.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/15.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/16.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/16.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/17.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/17.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/18.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/18.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/19.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/19.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/20.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/20.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/21.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/21.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/22.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/22.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/23.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/23.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/24.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/24.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/25.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/25.png" class="img-responsive" alt="Thumb Knit"></a></li>
                          <li><a href="resource/img/knit/women/26.png" data-lightbox="knitWomen" data-title="Garment: Knit"><i class="fa fa-camera-retro fa-3x"></i><img src="resource/img/knit/women/26.png" class="img-responsive" alt="Thumb Knit"></a></li>

                      </ul>
                  </div>
              </div>
            </div>
        </div>

    </div>
</section>


<?php include_once "resource/particles/footer.php";?>

<script src="resource/js/jquery.min.js"></script>
<script src="resource/js/jquery.mousewheel.min.js"></script>
<script src="resource/js/norms.js"></script>
<script src="resource/js/bootstrap.min.js"></script>
<script src="resource/js/lightbox.js"></script>
<script>
    $(document).ready(function(){
      lightbox.option({
          'disableScrolling': true
      });

      $(window).on('scroll',function(){
        var wScroll = $(this).scrollTop();
        $('.tagContainer>img').css({
            '-webkit-transform': 'translate(0,-'+ wScroll/12 +'%)',
            'transform': 'translate(0,-'+ wScroll/12 +'%)'
        });
      });

        $('.tab-pane.active').find('.image-box li').each(function (indexNumber) {
            setTimeout(function () {
                $('.tab-pane.active').find('.image-box li').eq(indexNumber).addClass("animated fadeInUp");
            }, 130 * (indexNumber+1));
        });
        $('.nav-tabs-overClock li a').on('click',function() {
            $('.image-box li').each(function(){
              $('.image-box li').removeClass("animated fadeInUp");
            })
            var tab = $(this).attr('href');
            $(tab).find('.image-box li').each(function (eachIndex) {
              // console.log(eachIndex);
                  setTimeout(function () {
                  $(tab).find('.image-box li').eq(eachIndex).addClass("animated fadeInUp");
                }, 130 * (eachIndex+1));
            });
      });
  });
</script>
</body>
</html>
