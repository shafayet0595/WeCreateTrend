<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WeCreate TREND</title>
    <link rel="shortcut icon" href="resource/img/fav16.png">
    <link rel="stylesheet" href="resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/css/landPage.css"><!--lead---->
    <link rel="stylesheet" href="resource/css/themify-icons.css">
    <link rel="stylesheet" href="resource/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="resource/css/normalise.css">
    <link rel="stylesheet" href="resource/css/animate.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
</head>
<body id="scroll_home">
<?php include_once "resource/particles/loader.php"; ?>

<div class="floated_Nav_links clearfix visible-xs">
    <a href="" class="remove_floater" data-toggle="removeFloater"><i class="ti-close"></i></a>
    <ul class="">
        <li><a href="index.php">Home</a></li>
        <li><a data-toggle="collapse" href="#extlinks" aria-expanded="false" aria-controls="extlinks">Products <i class="fa fa-angle-down"></i></a>
            <ul class="collapse" id="extlinks">
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="index.php#scroll_contact">Contact</a></li>
    </ul>
    <div class="container-fluid topNav" style="text-align: center; border: none">
        <ul class="col-md-6 col-sm-6">
            <li>
                <i class="fa fa-phone"></i> +880-31-622804
            </li>
            <li>
                <i class="fa fa-fax"></i> +880-31-624547
            </li>
            <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
        </ul>
        <ul class="col-md-6 col-sm-6">
            <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                kotwali, Chittagong,
                Bangladesh.</li>
        </ul>
    </div>
</div>
<div class="respoBodyholder visible-xs"></div>
<div class="container-fluid topNav hidden-xs">
    <ul class="col-md-6 col-sm-6" style="text-align: left">
        <li>
            <i class="fa fa-phone"></i> +880-31-622804
        </li>
        <li>
            <i class="fa fa-fax"></i> +880-31-624547
        </li>
        <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
    </ul>
    <ul class="col-md-6 col-sm-6" style="text-align: right">
        <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
            kotwali, Chittagong,
            Bangladesh.</li>
    </ul>
</div>
<div class="container-fluid navigation clearfix">
    <div class="logoHolder">
        <a href="" class="menuBar visible-xs">
            <span></span>
        </a>
        <img src="resource/img/colorLogo.png" alt="WCT logo">
    </div>
    <ul class="navigationHold pull-right hidden-xs" id="top-menu">
        <li><a class="page-scroll" href="index.php#scroll_home">Home</a></li>
        <li class="productList">
            <a class="page-scroll" href="index.php#scroll_product">Products</a>
            <ul>
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li class="active"><a href="#scroll_home">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a class="page-scroll" href="index.php#scroll_contact">Contact</a></li>
    </ul>
</div>

<!--/****************************** header *********************************/-->
<section class="container-fluid tagContainer" style="padding: 0;">
    <img src="resource/img/pattern.png" alt="header image">
    <h1><i class="ti-package"></i> Factories</h1>
    <h5>home / factories</h5>
</section>

<section class="container-fluid" style="padding:70px 0" id="factories">
        <div class="container factory clearfix">
            <div class="factory-blurb col-md-8 clearfix">
                <h2><i class="ti-ink-pen"></i> S.K.R Attire Ltd.</h2>
                <blockquote>
                    S.K.R Attire LTD Garments is a well established company specializing in the manufacturing and exporting of high fashion garments since 2007.
                    From the inception, we have been producing a wide range of clothings for mens, ladies and children.
                </blockquote>
                <h4 style="text-align: left"><i>Main Garment</i></h4>
                <ul id="category" style="text-align: left">
                    <li>Knit</li>
                    <li>Woven</li>
                </ul>
            </div>

            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-industry fa-2x"></i>
                <p><span class='numscroller' data-slno='1' data-min='1' data-max='35000' data-delay='5' data-increment='200'>35000</span> square feet</p>
                <h3>Factory Space</h3>
            </div>
            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-cogs fa-2x"></i>
                <p><span class='numscroller' data-slno='2' data-min='1' data-max='240' data-delay='5' data-increment='10'>240</span> sets</p>
                <h3>Machineries</h3>
            </div>
            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-cubes fa-2x"></i>
                <p>
                    <span class='numscroller' data-slno='3' data-min='1' data-max='16000' data-delay='5' data-increment='100'>16000</span> Dz / Month.
                </p>
                <h3>Capacity</h3>
            </div>

            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-user-plus fa-2x"></i>
                <p>
                    <span class='numscroller' data-slno='4' data-min='1' data-max='500' data-delay='5' data-increment='10'>500</span> persons
                </p>
                <h3>Man Power</h3>
            </div>
        </div>

        <span class="dispatch clearfix"><i class="ti-ink-pen"></i></span>

        <div class="container factory clearfix">
            <div class="factory-blurb col-md-8 clearfix">
                <h2><i class="ti-ink-pen"></i> Syed knit wears Ltd.</h2>
                <blockquote>
                    Syed Knit Wears Ltd., established on 16th June, 2005; is a sister concern of “Clifton Group”-one of the largest garments industry in Bangladesh. It is a unique concern, manufacturing quality knitwear, all items of readymade garments for all ages in tune with the latest American, Canadian & European Fashion.
                </blockquote>
                <h4 style="text-align: left"><i>Main Garment</i></h4>
                <ul id="category" style="text-align: left">
                    <li>Knit</li>
                </ul>
            </div>

            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-industry fa-2x"></i>
                <p><span class='numscroller' data-slno='5' data-min='1' data-max='40958' data-delay='5' data-increment='200'>40958</span> square feet</p>
                <h3>Factory Space</h3>
            </div>
            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-cogs fa-2x"></i>
                <p><span class='numscroller' data-slno='6' data-min='1' data-max='332' data-delay='5' data-increment='10'>332</span> sets</p>
                <h3>Machineries</h3>
            </div>
            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-cubes fa-2x"></i>
                <p>
                    <span class='numscroller' data-slno='7' data-min='1' data-max='20000' data-delay='5' data-increment='100'>20000</span> Dz / Month.
                </p>
                <h3>Capacity</h3>
            </div>

            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-user-plus fa-2x"></i>
                <p>
                    <span class='numscroller' data-slno='8' data-min='1' data-max='460' data-delay='5' data-increment='10'>460 </span> persons
                </p>
                <h3>Man Power</h3>
            </div>
        </div>

        <span class="dispatch clearfix"><i class="ti-ink-pen"></i></span>

        <div class="container factory clearfix">
            <div class="factory-blurb col-md-8 clearfix">
                <h2><i class="ti-ink-pen"></i> Chistiya Apparels Limited</h2>
                <blockquote>
                    Chistiya Apparels Limited, is one of the most successful textile manufacturers in Bangladesh; a country with great potential in the field of garment- manufacturing. Chistiya has acquired a versatile production chain to cater flexible orders in shorter lead-time. Being an industry- standard certificate holder, Chistiya assures the perfect blend of quality and efficiency.
                </blockquote>
                <h4 style="text-align: left"><i>Main Garment</i></h4>
                <ul id="category" style="text-align: left">
                    <li>Knit</li>
                </ul>
            </div>

            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-industry fa-2x"></i>
                <p><span class='numscroller' data-slno='5' data-min='1' data-max='12000' data-delay='5' data-increment='100'>12000</span> square feet</p>
                <h3>Factory Space</h3>
            </div>
            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-cogs fa-2x"></i>
                <p><span class='numscroller' data-slno='6' data-min='1' data-max='182' data-delay='5' data-increment='10'>182</span> sets</p>
                <h3>Machineries</h3>
            </div>
            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-cubes fa-2x"></i>
                <p>
                    <span class='numscroller' data-slno='7' data-min='1' data-max='52000' data-delay='5' data-increment='200'>52000</span> Dz / Month.
                </p>
                <h3>Capacity</h3>
            </div>

            <div class="detail col-md-4 col-sm-4 col-xs-12">
                <i class="fa fa-user-plus fa-2x"></i>
                <p>
                    <span class='numscroller' data-slno='8' data-min='1' data-max='273' data-delay='5' data-increment='10'>273</span> persons
                </p>
                <h3>Man Power</h3>
            </div>
        </div>
</section>


<?php include_once "resource/particles/footer.php";?>

<script src="resource/js/jquery.min.js"></script>
<script src="resource/js/jquery.mousewheel.min.js"></script>
<script src="resource/js/norms.js"></script>
<script src="resource/js/bootstrap.min.js"></script>
<script src="resource/js/numscroller.js"></script>
<script>
$(window).on('scroll',function() {
    var wScroll = $(this).scrollTop();
    $('.tagContainer>img').css({
        '-webkit-transform': 'translate(0,-'+ wScroll/12 +'%)',
                'transform': 'translate(0,-'+ wScroll/12 +'%)'
    });
});
</script>
</body>
</html>
