<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WeCreate TREND</title>
    <link rel="shortcut icon" href="resource/img/fav16.png">
    <link rel="stylesheet" href="resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/css/landPage.css"><!--lead---->
    <link rel="stylesheet" href="resource/css/themify-icons.css">
    <link rel="stylesheet" href="resource/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="resource/css/normalise.css">
    <link rel="stylesheet" href="resource/css/animate.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
</head>
<body id="scroll_home">
<?php include_once "resource/particles/loader.php"; ?>

<div class="floated_Nav_links clearfix visible-xs">
    <a href="" class="remove_floater" data-toggle="removeFloater"><i class="ti-close"></i></a>
    <ul class="">
        <li><a href="index.php">Home</a></li>
        <li><a data-toggle="collapse" href="#extlinks" aria-expanded="false" aria-controls="extlinks">Products <i class="fa fa-angle-down"></i></a>
            <ul class="collapse" id="extlinks">
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li><a href="service.php">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="index.php#scroll_contact">Contact</a></li>
    </ul>
    <div class="container-fluid topNav" style="text-align: center; border: none">
        <ul class="col-md-6 col-sm-6">
            <li>
                <i class="fa fa-phone"></i> +880-31-622804
            </li>
            <li>
                <i class="fa fa-fax"></i> +880-31-624547
            </li>
            <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
        </ul>
        <ul class="col-md-6 col-sm-6">
            <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                kotwali, Chittagong,
                Bangladesh.</li>
        </ul>
    </div>
</div>
<div class="respoBodyholder visible-xs"></div>
<div class="container-fluid topNav hidden-xs">
    <ul class="col-md-6 col-sm-6" style="text-align: left">
        <li>
            <i class="fa fa-phone"></i> +880-31-622804
        </li>
        <li>
            <i class="fa fa-fax"></i> +880-31-624547
        </li>
        <li style="border: none;"><i class="fa fa-envelope"></i> &nbsp; info@wecreatetrend.com</li>
    </ul>
    <ul class="col-md-6 col-sm-6" style="text-align: right">
        <li><i class="fa fa-map-marker"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
            kotwali, Chittagong,
            Bangladesh.</li>
    </ul>
</div>
<div class="container-fluid navigation clearfix">
    <div class="logoHolder">
        <a href="" class="menuBar visible-xs">
            <span></span>
        </a>
        <img src="resource/img/colorLogo.png" alt="WCT logo">
    </div>
    <ul class="navigationHold pull-right hidden-xs" id="top-menu">
        <li><a class="page-scroll" href="index.php#scroll_home">Home</a></li>
        <li class="productList">
            <a class="page-scroll" href="index.php#scroll_product">Products</a>
            <ul>
                <li><a href="knitwears.php">Knit</a></li>
                <li><a href="wovenwears.php">Woven</a></li>
                <li><a href="">Sweaters</a></li>
                <li><a href="">Others</a></li>
            </ul>
        </li>
        <li class="active"><a href="#scroll_home">Services</a></li>
        <li><a href="factory.php">Factories</a></li>
        <li><a href="about.php">About</a></li>
        <li><a class="page-scroll" href="index.php#scroll_contact">Contact</a></li>
    </ul>
</div>

<!--/****************************** header *********************************/-->
<section class="container-fluid tagContainer" style="padding: 0;">
    <img src="resource/img/pattern.png" alt="header image">
    <h1><i class="ti-briefcase"></i> Services</h1>
    <h5>home / services</h5>
</section>

<section class="container-fluid" style="padding:0" id="service">
        <div class="container faq">
            <h1><i class="fa fa-question"></i> Faq</h1>
            <div class="panel-group col-md-9 col-sm-9" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="query" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Q. 1- Our company has never purchased through an intermediary. We have always purchased directly. How easy is it to buy through you ?
                                <i class="pull-right fa fa-angle-down rotated"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <blockquote>
                                <i class="fa fa-angle-double-right"></i> It makes lot of sense for you to buy through us. We provide all the value added services. We have an extensive designing and sampling department which can offer you a uni-collection to choose from and research and development department which constantly investing time and efforts in finding new fashions and factories it can work with. We offer you world class products at competitive price.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="query collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <span>Q: 2- How do you claim that your prices are more competitive than Direct Buying?</span>
                                <i class="pull-right fa fa-angle-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <blockquote>
                                <i class="fa fa-angle-double-right"></i>  As we are continuously working with all types of manufacturers from different places, it is easily possible for us to give you competitive price by match you right supplier as per your requirements. Also we assist the manufacturers to source the raw materials and accessories cheaply and qualitely from around the world. Further, it is because of a simple philosophy build into our corporate culture - "avoid waste and maximize productivity" through our value added services. At most care is taken to make sure that mistakes are avoided before they can occur. Each and every process will be allowed for bulk production only after our prior approvals. Hence we claim to be the most competitive.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="query collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Q. 3- How can you assure us for the time delivery?
                                <i class="pull-right fa fa-angle-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <blockquote>
                                <i class="fa fa-angle-double-right"></i>  It is very simple. Before confirm the order to a supplier, Dean assesses the factories in terms of product range, quality of the products made, production capacity, technology orientations, working condition, financial capacity, man power, managerial efficiency and their overall facilities. After selection of a vendor, our continuos value added services from sampling to shipment makes us easy to keep-up the delivery schedule.
                            </blockquote>
                        </div>
                    </div>
                </div>



                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a class="query collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Q. 4- How do you assure that the quality of our consignment will be as per our specifications?
                                <i class="pull-right fa fa-angle-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">
                            <blockquote>
                                <i class="fa fa-angle-double-right"></i>  Every stage of the production processes are monitored thoroughly by us in systematic way. Finally the final inspection perform according to your required AQL level. The detailed final inspection is based on the specification of the principle and cover the criteria such as design / style, shrinkage, accessories, appearance, markings, color, labeling, material, assortments, workmanship, measurements, packing, etc. The reports on the multi-stage inspections are transmitted to the buyers by fax / e.mail. Only after the buyers approval Dean will allow the suppliers to ship the consignment.
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                            <a class="query collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Q. 5- How to pay your commission?
                                <i class="pull-right fa fa-angle-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">
                            <blockquote>
                                <i class="fa fa-angle-double-right"></i>  While opening the L/C by adding our commission along with the garment price and that can be mentioned in the L/C itself which will come to our account at the time of the bill payment. Otherwise you can pay us directly through wire transfer after the shipment.
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                        <h4 class="panel-title">
                            <a class="query collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                Q. 6- We would like to explore business opportunities?
                                <i class="pull-right fa fa-angle-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                        <div class="panel-body">
                            <blockquote>
                                <i class="fa fa-angle-double-right"></i>  Contact us with your inquiries. We can meet you in your office in your country at your convenience to discuss in detail and can give you a presentation of our latest collections.
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="whyUs">
                    <h1><i class="fa fa-dollar"></i> Our Charges</h1>
                    <blockquote>
                        Most importers have <b>found it cheaper</b> to work with us because we are able to source products much cheaper than their direct buying from the manufacturers and <b>without any risk</b>. Our charges are generally a <b>small percentage of the FOB value of the shipment</b>.
                        <br><br>
                        We generally source goods at a lower cost than most other buying offices in Bangladesh. So, <b>our commission tends to pay for itself</b>.
                        <br><br>
                        We <b>reduce the cost of buying, cost of managing quality and cost of assuring deliveries</b>. And, our customers get to know new developments, new products and new supply base from this part of the world.
                    </blockquote>
                </div>

                <div class="example">
                    <h3>Example</h3>
                    <blockquote>
                            If you were to place an order for 10,000 T-shirts: <br>

                            <b>Buying DIRECT:</b> The supplier quote is $ 4.00 per T-shirt
                            <b>Total cost FOB: 4.00 x 10,000 = $ 40,000 . 00</b>

                            Buying through <b>DEAN TEXTILES:</b> We source the same product at <b>$ 3.50 per T-shirt</b>
                        <br>
                        <br>
                        <ul>
                            <li>Total Cost FOB:  3.50 x 10,000 = $ 35,000 . 00</li>
                            <li>Total Cost FOB: 3.50 x 10,000 = $ 35,000 . 00</li>
                            <li>Your total Cost: 35,000 + 1,750 = $ 36,750 . 00</li>
                            <li>Your savings = 40,000 - 36,750 = $ 3,250 . 00</li>
                        </ul>

                        <br>
                        You <b>save $ 3,250 on your FOB import cost</b>, in addition you will get guarantee for your imports and our services including <b>Quality Inspections, Production Monitoring, Updates and most important Timely Delivery</b>.
                        <br>
                        All this at a cost which is actually a saving for our buyers!
                    </blockquote>
                    <h1><i class="fa fa-star-half-empty"></i> GIVE US A TRY</h1>
                    <blockquote>
                        You may have heard about the difficulties faced by importers in sourcing High Quality Garments from Bangladesh on consistent basis. It is easily possible by using an <b>experienced, quality conscious, dedicated buying office like WE CREATE TREND LTD.SERVICE , GIVE US A TRY!</b>
                        <br>
                        Get us to sample a product for you. You will get an idea of our resources and the quality of our vendor base and services. We promise your value for money and trouble free sourcing from one of the growing and competitive economies in the world.
                    </blockquote>

                </div>

            </div>

            <div class="sideInfo col-md-3 col-sm-3">
                <h4><i class="ti-info-alt"></i> Contact Info.</h4>
                <ul style="padding: 0;">
                    <li>
                        <i class="fa fa-map-o"></i>&nbsp; 39 Hossain Shahid Sarwardi Road,
                        kotwali, Chittagong,
                        Bangladesh.
                    </li>
                    <li>
                        <i class="fa fa-envelope"></i>&nbsp; info@wecreatetrend.com
                    </li>
                    <li>
                        <i class="fa fa-fax"></i>&nbsp; +880-31-624547
                    </li>
                    <li>
                        <i class="fa fa-phone"></i>&nbsp; +880-31-622804
                    </li>
                </ul>


                <h4><i class="ti-tag"></i> Major Buyers</h4>
                <ul style="padding:0; list-style: none">
                    <li> <b>Canada</b> <i class="fa fa-angle-right"></i> YM</li>
                    <li> <b>Mexico</b> <i class="fa fa-angle-right"></i> coppel</li>
                    <li> <b>U.K.</b> <i class="fa fa-angle-right"></i> Double two</li>
                    <li> <b>Germany</b> <i class="fa fa-angle-right"></i> Kik-textilien etc.</li>
                    <li> <b>Belgium</b> <i class="fa fa-angle-right"></i> Vegotex, Hanes etc.</li>
                    <li> <b>USA</b> <i class="fa fa-angle-right"></i> Marvyns , Green dog , carters etc.</li>
                    <li> <b>france</b> <i class="fa fa-angle-right"></i> Defimode , Sodiref , Vetura samino etc.</li>
                </ul>

                <h4 class="client-header"> <i class="ti-wallet"></i> Bank Detail</h4>
                <ul style="padding: 0;">
                    <li><b>Name <i class="fa fa-angle-right"></i> </b> Southeast Bank Limited</li>
                    <li><b>Branch <i class="fa fa-angle-right"></i> </b> CDA Avenue Branch</li>
                    <li><b>Address <i class="fa fa-angle-right"></i>  </b> Ali Villa, 1640/1861 (New), CDA Avenue, Asian Highway,East Nasirabad, Chittagong, Bangladesh</li>
                    <li><b>SWIFT: SEBDBDDHACDA</b></li>
                </ul>
            </div>
        </div>
</section>


<?php include_once "resource/particles/footer.php";?>

<script src="resource/js/jquery.min.js"></script>
<script src="resource/js/jquery.mousewheel.min.js"></script>
<script src="resource/js/norms.js"></script>
<script src="resource/js/bootstrap.min.js"></script>
<script>
    $(window).on('scroll',function() {
        var wScroll = $(this).scrollTop();
        $('.tagContainer>img').css({
            '-webkit-transform': 'translate(0,-'+ wScroll/12 +'%)',
                    'transform': 'translate(0,-'+ wScroll/12 +'%)'
        });
    });

    $(document).ready(function () {
        $('.query').each(function () {
            $(this).on('click',function () {
                if($(this).hasClass("collapsed")){
                    $('.query i').not(this).removeClass("rotated");
                    $(this).find('i').toggleClass("rotated");
                }else{
                    $(this).find('i').toggleClass("rotated");
                }
            });
        });
    });

</script>
</body>
</html>
